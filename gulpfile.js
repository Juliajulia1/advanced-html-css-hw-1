import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;

import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import htmlImport from "gulp-html-import";
import htmlMin from "gulp-htmlmin";
import htmlBemValidator from "gulp-html-bem-validator";
import changed from "gulp-changed";
import { compareContents } from "gulp-changed";
import gGcssMedia from "gulp-group-css-media-queries";
// import webp from 'gulp-webp';

import bsc from "browser-sync";
const browserSync = bsc.create();

const htmlTaskHandler = () => {
	return src("./src/html/*.html")
	.pipe(changed("./dist", { hasChanged: compareContents }))
		.pipe(htmlImport('./src/html/components/'))
		.pipe(htmlBemValidator())
		.pipe(htmlMin({ collapseWhitespace: true }))
		.pipe(dest("./dist"));
};

const cssTaskHandler = () => {
	return src("./src/scss/style.scss")
	.pipe(changed("./dist/css", { hasChanged: compareContents }))
		.pipe(sass().on("error", sass.logError))
		.pipe(gGcssMedia())
		.pipe(autoprefixer())
		.pipe(csso())
		.pipe(dest("./dist/css"))
		.pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
	return src("./src/img/**/*.*")
	.pipe(changed("./dist/img/**/*", { hasChanged: compareContents }))
		// .pipe(webp())
		.pipe(imagemin({ verbose: true, quality: 90 }))
		.pipe(dest("./dist/img"));
};

// const fontTaskHandler = () => {
// 	return src("./src/fonts/**/*.*").pipe(dest("./dist/fonts"));
// };

const cleanDistTaskHandler = () => {
	return src("./dist", { read: false, allowEmpty: true }).pipe(
		clean({ force: true })
	);
};

const browserSyncTaskHandler = (done) => {
	browserSync.init({
		server: {
			baseDir: "./dist",
		}
	});

	watch("./src/scss/**/*.scss").on(
		"all",
		series(cssTaskHandler, browserSync.reload)
	);
	watch("./src/html/**/*.html").on(
		"change",
		series(htmlTaskHandler, browserSync.reload)
	);
	watch("./src/img/**/*").on(
		"all",
		series(imagesTaskHandler, browserSync.reload)
	);
	done();
};

export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
// export const font = fontTaskHandler;
export const images = imagesTaskHandler;

export const build = series(
	cleanDistTaskHandler,
	parallel(htmlTaskHandler, cssTaskHandler, imagesTaskHandler) //fontTaskHandler, 
);
export const dev = series(build, browserSyncTaskHandler);
